@echo off

IF "%1" NEQ "list" GOTO Continue
echo ============== Projects ==============
dir /a:d /b %projectpath%
echo ======================================
GOTO End

:Continue
set projectname = %1
cd /d %~dp0

If "%1"=="" (
    echo "ERROR: Please enter a projectname"
) else (
    python open.py %1
)


:End