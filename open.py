import sys
import os

# Setup
projectname = str(sys.argv[1])
projectpath = os.environ.get('projectpath')
_projectdir = projectpath + '\\' + projectname

if os.path.isdir(_projectdir):
    os.chdir(_projectdir)
    os.system('code .')
else:
    print(f"404 - {projectname} not found.")
    