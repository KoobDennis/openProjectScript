# Open Project Script 
a script to open a project in visual studio code by command

# Setup 
1. Edit the `"setup.bat"`
```batch
setx projectpath <BASEPROJECTPATH>
```
   - Enter a base project path. 
2. Execute the batch file to set enviornment variables for the token and project path. 
3. Add the project folder to the PATH environment variable

# Usage
```
open <projectname>
```
Opens visual Studio Code into the projectfolder
```
open list
```
Output:
```
============== Projects ==============
Project A
Project B
Project C
Project D
======================================
```
List all folders in the project base path folder set in the enviornment variable